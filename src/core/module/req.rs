extern crate futures;
extern crate hyper;
extern crate tokio_core;
use self::futures::{Future,Stream};
use self::hyper::Client;
use self::tokio_core::reactor::Core;
use std::io::{self, Write};
use std::str;
// Потоки
use std::thread;

pub struct Requests<'a> {
    pub url_arr: &'a Vec<String>,
}


pub trait Methods {
    fn client<'b>(&'b self);
    fn parse<'b>(&'b self, url: String)-> Result<(), Box<::std::error::Error>>;
}

    
impl<'a> Methods for Requests<'a> {
    // Метод который отвечает за запрос к сайту
    fn client<'b>(&'b self) {
        let items_clone: Vec<String> = self.url_arr.clone();
        // Перебираем наши сайты из файла в несколько потоков
        for iters in items_clone {
            let _ = &self.parse(iters.to_string()).expect("http client errored");
        }
        
    }
    // Request к сайтам происходить тут
    fn parse<'b>(&'b self, url: String) -> Result<(), Box<::std::error::Error>>  {
        thread::spawn(move || {
            let mut core = Core::new().unwrap();
            let client = Client::new(&core.handle());
            let url_parse = url.parse().unwrap();
            // Делаем запрос и замыкаем результат
            let work = client.get(url_parse).and_then(|res| {
                println!("Response: {}", res.status());
                res.body().concat2()
            });
            let _ = core.run(work).unwrap();
            // println!("GET: {}", str::from_utf8(&got)?);
        }).join();

        Ok(())
    }

}
