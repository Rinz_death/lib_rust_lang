use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;
// use std;

// Преобразуем строку в массив
fn string_to_array(string: String) -> Vec<String> {
    let split: Vec<String> = string.split(',').map(|s| s.to_string()).collect();;
    return split
}
// Чтение файла
pub fn read_file(file: String) -> Vec<String> {
    let file = File::open(file).expect("file not found");
    let mut buf_reader = BufReader::new(file);
    let mut contents = String::new();
    buf_reader.read_to_string(&mut contents).expect("something went wrong reading the file");
    return string_to_array(contents);;
}