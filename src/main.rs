// extern crate server;
extern crate server;
use server::core::module::req;
// Методы у request'a
use server::core::module::req::Methods;
// Чтение с файла
use server::core::module::read_file;
fn main() {
    // server::core::server::server::server();
    let array_url = read_file::read_file("../site.txt".to_string());
    // Server
    req::Requests{url_arr: &array_url}.client();
    // println!("{}",array_url[0]);
}
